public class LifeGamePatternGenerator{

  public static void createBeehive(int startX , int startY , LifeGame game){
    game.setAliveCell(startX+1,startY);
    game.setAliveCell(startX+2,startY);
    game.setAliveCell(startX,startY+1);
    game.setAliveCell(startX+3,startY+1);
    game.setAliveCell(startX+1,startY+2);
    game.setAliveCell(startX+2,startY+2);
  }

  public static void createMiddleSpaceship(int startX , int startY , LifeGame game){
    game.setAliveCell(startX+2,startY);
    game.setAliveCell(startX+3,startY);
    game.setAliveCell(startX,startY+1);
    game.setAliveCell(startX+4,startY+1);
    game.setAliveCell(startX+5,startY+2);
    game.setAliveCell(startX,startY+3);
    game.setAliveCell(startX+5,startY+3);
    game.setAliveCell(startX+1,startY+4);
    game.setAliveCell(startX+2,startY+4);
    game.setAliveCell(startX+3,startY+4);
    game.setAliveCell(startX+4,startY+4);
    game.setAliveCell(startX+5,startY+4);
  }

  public static void createPuffertrain(int startX , int startY , LifeGame game){
    game.setAliveCell(startX+6 , startY+1 );
    game.setAliveCell(startX+7 , startY+1 );
    game.setAliveCell(startX+8 , startY+1 );
    game.setAliveCell(startX+9 , startY+1 );

    game.setAliveCell(startX+5 , startY+2 );
    game.setAliveCell(startX+5 , startY+2 );
    game.setAliveCell(startX+9 , startY+2 );

    game.setAliveCell(startX+9 , startY+3 );

    game.setAliveCell(startX+1 , startY+4 );
    game.setAliveCell(startX+2 , startY+4 );
    game.setAliveCell(startX+5 , startY+4 );
    game.setAliveCell(startX+8 , startY+4 );

    game.setAliveCell(startX+1 , startY+5 );
    game.setAliveCell(startX+2 , startY+5 );
    game.setAliveCell(startX+3 , startY+5 );

    game.setAliveCell(startX+1 , startY+6 );
    game.setAliveCell(startX+2 , startY+6 );
    game.setAliveCell(startX+5 , startY+6 );
    game.setAliveCell(startX+8 , startY+6 );

    game.setAliveCell(startX+9 , startY+7 );

    game.setAliveCell(startX+5 , startY+8 );
    game.setAliveCell(startX+5 , startY+8 );
    game.setAliveCell(startX+9 , startY+8 );

    game.setAliveCell(startX+6 , startY+9 );
    game.setAliveCell(startX+7 , startY+9 );
    game.setAliveCell(startX+8 , startY+9 );
    game.setAliveCell(startX+9 , startY+9 );
  }
}
