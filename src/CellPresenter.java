import java.awt.Graphics;
import java.awt.Color;

class CellPresenter {

  private Cell cell;

  CellPresenter(Cell cell) {
    this.cell = cell;
  }

  void present(Graphics g) {
    if(cell.isAlive()) {
      g.setColor(Color.BLUE);
    } else {
      g.setColor(Color.WHITE);
    }
    g.fillRect(200 + (cell.getX() * 10), 50 + (cell.getY() * 10), 10, 10);
  }
}
