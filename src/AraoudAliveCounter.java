import java.util.List;
import java.util.ArrayList;

public class AraoudAliveCounter{

  public static int counter(int targetX , int targetY ,  Cells cells){
    int count = 0;
    for (int x = targetX-1 ; x <= targetX+1 ; x++ ){
      for (int y = targetY-1 ; y <= targetY+1 ; y++ ){
        if ((x != targetX || y != targetY ) && cells.at(x,y).isAlive()){
          count++;
        }
      }
    }
    return count;
  }
}
