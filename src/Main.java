public class Main {

  public static void main(String[] args) {
    LifeGame game = new LifeGame(100, 60);

    LifeGamePatternGenerator.createPuffertrain(50,30,game);
    LifeGamePatternGenerator.createBeehive(80,30,game);
    LifeGamePatternGenerator.createMiddleSpaceship(20,30,game);

    game.start();
  }
}
