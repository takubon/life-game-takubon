public class Cell{
  private boolean status = false;
  private int x;
  private int y;

  public Cell(int x , int y ){
    this.x = x;
    this.y = y;
  }
  public boolean isAlive(){
    return status;
  }

  public void setStatus(boolean status){
    this.status = status;
  }

  public int getX(){
    return x;
  }
  public int getY(){
    return y;
  }
}
