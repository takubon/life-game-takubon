public class Result{

  private Cell cell;
  private int aliveCount = 0;

  public Result(Cell cell , int aliveCount){
    this.cell = cell;
    this.aliveCount = aliveCount;
  }
  public void update(){
    if (isBirth()){
      cell.setStatus(true);
    }else if (isDeath()){
      cell.setStatus(false);
    }
  }

  private boolean isBirth(){
    return aliveCount == 3;
  }

  private boolean isDeath(){
    return aliveCount <= 1 || aliveCount >= 4;
  }
}
