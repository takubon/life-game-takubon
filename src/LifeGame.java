public class LifeGame {

  private Cells cells;

  public LifeGame(int width, int height) {
    cells = CellsFactory.create(width, height);
  }

  public void setAliveCell(int x, int y) {
    cells.at(x, y).setStatus(true);
  }

  public void start() {
    LifeGameFrame frame = new LifeGameFrame(cells);
    frame.setVisible(true);
  }
}
