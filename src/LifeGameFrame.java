import java.util.List;
import java.util.ArrayList;
import java.awt.Graphics;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Button;
import java.awt.BorderLayout;
import java.awt.Toolkit;
import java.awt.Dimension;
import java.awt.Image;
import javax.swing.JFrame;

class LifeGameFrame extends JFrame implements ActionListener {

  private List<CellPresenter> presenters = new ArrayList<CellPresenter>();
  private Cells cells;
  private boolean canSimulate = false;
  private Thread simulator = null;
  private Image offScreen;

  LifeGameFrame(Cells cells) {
    this.cells = cells;
    for(Cell cell : this.cells.getAllCell()) {
      presenters.add(new CellPresenter(cell));
    }
    setBounds(0, 0, 1500, 1000);
    setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    this.presenters = presenters;
    Button button = new Button("simulation");
    button.addActionListener(this);
    getContentPane().add(button, BorderLayout.SOUTH);
  }

  @Override
  public void update(Graphics g) {
    paint(g);
  }

  @Override
  public void paint(Graphics g) {
    if(canSimulate) {
      for(CellPresenter cell : presenters) {
        cell.present(g);
      }
    }
  }

  @Override
  public void actionPerformed(ActionEvent event) {
    simulate();
  }

  private synchronized void simulate() {
    if(simulator == null) {
      canSimulate = true;
      simulator = simulator();
      simulator.start();
    }
  }

  private Thread simulator() {
    Thread thread = new Thread() {
      @Override
      public void run() {
        while(true) {
          draw();
          try {
            Thread.sleep(10);
            cells.update();
          } catch(InterruptedException e) {}
        }
      }
    };
    return thread;
  }

  private Image offScreen() {
    if(offScreen == null) {
      Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
      offScreen = createImage(dimension.width, dimension.height);
    }
    return offScreen;
  }

  private void draw() {
    Dimension dimension = Toolkit.getDefaultToolkit().getScreenSize();
    offScreen().getGraphics().clearRect(0, 0, dimension.width, dimension.height);
    paint(offScreen().getGraphics());
    getGraphics().drawImage(offScreen, 0, 0, this);
  }
}
