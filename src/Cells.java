import java.util.List;
import java.util.ArrayList;

public class Cells{

  private List<Cell> cells = new ArrayList<Cell>();
  public void update(){
    List<Result> results = new ArrayList<Result>();
    for(Cell c : cells){
      results.add(new Result(c , AraoudAliveCounter.counter(c.getX() , c.getY() , this)));
    }
    for(Result r : results){
      r.update();
    }
  }

  public Cell at(int x , int y){
    for(Cell c : cells){
      if(c.getX() == x && c.getY() == y){
        return c;
      }
    }
    return new Cell(0,0);
  }

  public void add(Cell cell){
    cells.add(cell);
  }

  public List<Cell> getAllCell(){
    return cells;
  }
}
