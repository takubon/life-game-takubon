import org.junit.Test;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertThat;
import static org.hamcrest.CoreMatchers.is;

public class CellsFactoryTest{

  @Test
  public void 生成(){
    Cells cells = CellsFactory.create(2,2);
    assertThat(4, is(cells.getAllCell().size()));
    assertFalse(cells.at(1,1).isAlive());
    assertFalse(cells.at(2,2).isAlive());
  }
}
