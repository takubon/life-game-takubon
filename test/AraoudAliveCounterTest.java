import org.junit.Test;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.assertTrue;
import static org.hamcrest.CoreMatchers.is;

public class AraoudAliveCounterTest{

  @Test
  public void ZERO(){
    Cells cells = CellsFactory.create(3,3);
    assertThat(0, is(AraoudAliveCounter.counter(2 ,2 ,cells)));
  }

  @Test
  public void Two(){
    Cells cells = CellsFactory.create(3,3);
    cells.at(1,1).setStatus(true);
    cells.at(2,2).setStatus(true);
    cells.at(3,3).setStatus(true);
    assertThat(2, is(AraoudAliveCounter.counter(2 ,2 ,cells)));
  }

  @Test
  public void Three(){
    Cells cells = CellsFactory.create(2,2);
    cells.at(2,1).setStatus(true);
    cells.at(2,2).setStatus(true);
    cells.at(1,2).setStatus(true);
    assertThat(3, is(AraoudAliveCounter.counter(1 ,1 ,cells)));
  }
}
