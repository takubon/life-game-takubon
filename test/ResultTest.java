import org.junit.Test;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;


public class ResultTest{

  @Test
  public void 過疎(){
    Cell cell = prepareCell(true , 1 );
    assertFalse(cell.isAlive());
  }

  @Test
  public void 過密(){
    Cell cell = prepareCell(true , 4 );
    assertFalse(cell.isAlive());
  }

  @Test
  public void 誕生(){
    Cell cell = prepareCell(false , 3 );
    assertTrue(cell.isAlive());
  }

  @Test
  public void 維持(){
    Cell cell = prepareCell(false , 2 );
    assertFalse(cell.isAlive());
  }

  private Cell prepareCell(boolean status , int counter){
    Cell cell = new Cell(1,1);
    cell.setStatus(status);
    Result result = new Result(cell , counter );
    result.update();
    return cell;
  }

}
