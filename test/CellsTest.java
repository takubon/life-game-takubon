import org.junit.Test;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

public class CellsTest {

  @Test
  public void 過疎(){

    Cells cells = CellsFactory.create(2,2);
    cells.update();
    assertFalse(cells.at(1,1).isAlive());
  }

  @Test
  public void 誕生(){

    Cells cells = CellsFactory.create(2,2);
    cells.at(1,2).setStatus(true);
    cells.at(2,1).setStatus(true);
    cells.at(2,2).setStatus(true);
    cells.update();
    assertTrue(cells.at(1,1).isAlive());
  }
}

